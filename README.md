```c
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#define MAX_TEXT 100

#define MAX_TEXT_DESXIF 5
#define MAX_VCARACTERS 100
#define MAX_REPETICIONS 100
#define BB while(getchar()!='\n')
#define MAX_CODIS 30
#define MAX_DIGITS 5
#define MAX_XIFRAT 400

int main()
{
//DECLARACIO
char text[MAX_TEXT+1],vCaracter[MAX_VCARACTERS+1],caracter,auxc,
xifrat[MAX_XIFRAT],textDesX[MAX_TEXT_DESXIF+1];
int iText, iCaracter,repeticions[MAX_REPETICIONS],iRepeticions, auxr,
i,iTextDesXif;
int maxr, n,u,iDigit, iXifrat;
char codis[MAX_CODIS+1][MAX_DIGITS+1]= {
"0","1","00","01","10","11","000","001","010","011","100"

,"101","110","111","0000","0001"

,

"0010","0011","0100","0101","0110"

,"0111","1000","1001","1010","1100"

,"1101","1110","1111"
};

//INIZIALITZACIO

printf("Escriu un text:");
scanf("%100[^\n]",text);
BB;

//inizialitzar el vector caractes a \0 i repeticins a 0
for(iCaracter=0; iCaracter<MAX_VCARACTERS; iCaracter++)
vCaracter[iCaracter]='\0';
for(iRepeticions=0; iRepeticions<MAX_REPETICIONS; iRepeticions++)
repeticions[iRepeticions]=0;
iText=0;

while(text[iText]!='\0')
{
//obtenir caracter posicio indextext de text
caracter=text[iText];
iCaracter=0;
//Buscar caracter
while(vCaracter[iCaracter]!='\0' &&

vCaracter[iCaracter]!=caracter)

{
iCaracter++;
}

if(vCaracter[iCaracter]=='\0')
{
vCaracter[iCaracter]=caracter;
}
else repeticions[iCaracter]++;

iText++;
}
//*********************************Text sense ordenar rep
*********************************

//ordenar repeticions
maxr=0;
while(vCaracter[maxr]!='\0') maxr++;
for(i=2; i<=maxr; i++)
{
for(iCaracter=0; iCaracter<=maxr-i ; iCaracter++)
{
if(repeticions[iCaracter]<repeticions[iCaracter+1])
{
auxr=repeticions[iCaracter];
repeticions[iCaracter]=repeticions[iCaracter+1];
repeticions[iCaracter+1]=auxr;

auxc=vCaracter[iCaracter];
vCaracter[iCaracter]=vCaracter[iCaracter+1];
vCaracter[iCaracter+1]=auxc;
}
}
}

u=0;
//*********************************Text ordenant
rep*********************************

printf("\nCaracter, N vegades que es repeteix, Codi assignat\n");

while(vCaracter[u]!='\0')
{
printf("%c -> %i -> %s \n",vCaracter[u],repeticions[u],

codis[u]);
u++;

}

iText=0;
iXifrat=0;

//*********************************XIFRAR*********************************
while(text[iText]!='\0')
{
iDigit=0;
iCaracter=0;
while(vCaracter[iCaracter]!=text[iText]) iCaracter++;
while(codis[iCaracter][iDigit]!='\0')
{
xifrat[iXifrat]=codis[iCaracter][iDigit];
iDigit++;
iXifrat++;
}
xifrat[iXifrat]='$';
iText++;
iXifrat++;

}
xifrat[iXifrat]='\0';

printf("\nText xifrat: -> %s\n",xifrat);

//*********************************DES
XIFRAR*********************************

// Inicialitzacions
iText=0;

printf ("\nIntrodueix el text compactat: \n");
scanf ("%100[^\n]", xifrat);

printf ("\nEl text desxifrat és: \n");

// Mentre no final
while (xifrat[iXifrat] != '\0') {

iTextDesXif=0;

// Saltar dollars
while (xifrat[iXifrat] == '$') iTextDesXif++;

// Copiar codi
while (xifrat[iXifrat] != '$' && xifrat[iXifrat] != '\0') {
textDesX[iTextDesXif]=xifrat[iXifrat];
iXifrat++;
iTextDesXif++;
}

textDesX[iTextDesXif]='\0';
iTextDesXif=0;
iXifrat=0;

// Comparar el codi copiat amb la resta de codis del vector codi

while (xifrat[iXifrat] != '\0' || codis[iTextDesXif] != '\0') {

if (xifrat[iXifrat]!= textDesX[iTextDesXif]) {
iTextDesXif=0;
iXifrat++;
} else iTextDesXif++;
}

// Imprimir el caràcter assignat al codi corresponent
printf ("%c", vCaracter[iXifrat]);

} // Fi mentre no final

return 0;
}


```